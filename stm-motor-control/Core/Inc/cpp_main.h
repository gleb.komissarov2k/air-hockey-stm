#ifndef CPP_MAIN_H_
#define CPP_MAIN_H_

#include "spi.h"
#include "tim.h"
#include "usart.h"
#include "gpio.h"

#ifdef __cplusplus
extern "C" {
#endif

void cpp_main();
void cpp_loop();

void DebugData(char message[]);
uint32_t millis();

#ifdef __cplusplus
}
#endif

#endif // CPP_MAIN_H_

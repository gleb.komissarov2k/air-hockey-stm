#ifndef ENCODER_H_
#define ENCODER_H_

#include <spi.h>
#include <gpio.h>
#include <string.h>
#include <usart.h>

#ifdef __cplusplus
extern "C" {
#endif

class AS5048A
{
public:
	SPI_HandleTypeDef* spiHandle;
	GPIO_TypeDef* gpioPort;
	uint16_t gpioPin;
	uint16_t old_position;

	AS5048A(SPI_HandleTypeDef* SpiHandle, GPIO_TypeDef* GpioPort, uint16_t GpioPin);
	bool CalcEvenParity(uint16_t address);

	uint16_t Read(uint16_t address);
	uint16_t ReadRawAngle();
	uint16_t AverageRawAngle();
};

#ifdef __cplusplus
}
#endif

#endif // ENCODER_H_

#ifndef MOTOR_H_
#define MOTOR_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "tim.h"

class Motors
{
public:
	// Motor A control signal timer
	TIM_HandleTypeDef *a_tim;
	uint16_t a_channel;

	// Motor B control signal timer
	TIM_HandleTypeDef *b_tim;
	uint16_t b_channel;

	uint16_t speed;
	uint16_t position;
	uint16_t max_position;

	Motors(TIM_HandleTypeDef *a_tim, uint16_t a_channel, TIM_HandleTypeDef *b_tim, uint16_t b_channel);

	uint16_t ConvertSpeedToCompare(uint16_t motor_speed);
	void DriveMotors(uint16_t motor_speed);

	void CalibrateController();
	void CalibrateHand();
	void ParseCommand(char buffer[]);

};

#ifdef __cplusplus
}
#endif

#endif // MOTOR_H_

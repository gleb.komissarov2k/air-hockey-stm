#include <encoder.h>

AS5048A::AS5048A(SPI_HandleTypeDef* SpiHandle, GPIO_TypeDef* GpioPort, uint16_t GpioPin)
{
	spiHandle = SpiHandle;
	gpioPort = GpioPort;
	gpioPin = GpioPin;
	uint32_t old_position = AverageRawAngle();

	HAL_GPIO_WritePin(gpioPort, gpioPin, GPIO_PIN_SET);
}

bool AS5048A::CalcEvenParity(uint16_t address)
{
	int cnt = 0;

	for (int i = 0; i < 16; i++)
	{
		if (address & 1) cnt++;
		address >>= 1;
	}

	return cnt & 1;
}

uint16_t AS5048A::Read(uint16_t address)
{
	// Set "R/W" bit to 1 and add "Parity" bit
	uint16_t command = 0b0100000000000000;
	command |= address;
	command |= (uint16_t)CalcEvenParity(address) << 15;
	command = 0xFFFF;

	// Split command into 2 bytes
	uint8_t mosi[2] = { 0 };
	memcpy(mosi, &command, 2);

	uint8_t miso[2] = { 0 };

	// Send read address
	HAL_GPIO_WritePin(gpioPort, gpioPin, GPIO_PIN_RESET);
	HAL_SPI_Transmit(spiHandle, mosi, 2, HAL_TIMEOUT);
	HAL_GPIO_WritePin(gpioPort, gpioPin, GPIO_PIN_SET);

	// Receive read data
	HAL_GPIO_WritePin(gpioPort, gpioPin, GPIO_PIN_RESET);
	HAL_SPI_Receive(spiHandle, miso, 2, HAL_TIMEOUT);
	HAL_GPIO_WritePin(gpioPort, gpioPin, GPIO_PIN_SET);

	// Recombine data
	uint16_t reading = (( ( miso[0] & 0xFF) << 8 ) | ( mosi[1] & 0xFF )) & ~0xC000;
	return reading;
}

uint16_t AS5048A::ReadRawAngle()
{
	return AS5048A::Read(0x3FFF);
}

uint16_t AS5048A::AverageRawAngle()
{
	return 3;
	uint32_t angle = 0;

	for (int i = 0; i < 3; i++) {
		angle += ReadRawAngle();
	}

	return angle / 3;
}

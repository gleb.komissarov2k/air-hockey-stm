#include <motors.h>

#include <string.h>
#include <stdlib.h>

Motors::Motors(TIM_HandleTypeDef *motor_a_tim, uint16_t motor_a_channel, TIM_HandleTypeDef *motor_b_tim, uint16_t motor_b_channel)
{
	a_tim = motor_a_tim;
	a_channel = motor_a_channel;

	b_tim = motor_b_tim;
	b_channel = motor_b_channel;

	speed = 90;
}

uint16_t Motors::ConvertSpeedToCompare(uint16_t motor_speed)
{
	uint16_t high_time = 50 * motor_speed / 9 + 1000; // High time in microseconds
	uint16_t cnt_f = htim1.Init.Period * 50; // Counter Period * PWM frequency
	uint32_t compare = (high_time * cnt_f) / 1000000; // Time spent high * prescaler clock frequency / 10e-6
	return compare;
}

void Motors::DriveMotors(uint16_t motor_speed)
{
	speed = motor_speed;
	uint16_t compare = ConvertSpeedToCompare(speed);

	__HAL_TIM_SET_COMPARE(a_tim, a_channel, compare);
	__HAL_TIM_SET_COMPARE(b_tim, b_channel, compare);
}

void Motors::CalibrateController()
{
	for (uint8_t speed = 0; speed <= 180; speed = speed + 90)
	{
		DriveMotors(speed);
		HAL_Delay(500);
	}
	DriveMotors(90);
	return;
}

void Motors::CalibrateHand()
{
	// Move left
	DriveMotors(0); // Move left
	while (speed != 90) continue;

	// Move right
	position = 0;
	HAL_Delay(100); // Avoid full speed acceleration from 0 -> 180
	DriveMotors(180); // Head right

	// Stop motor
	while (speed != 90) continue;
	max_position = position;
	DriveMotors(90);
}

void Motors::ParseCommand(char buffer[])
{
	// Check for "Calibrate command
	if (!strcmp(buffer, "CAL")) {
		CalibrateController();
		CalibrateHand();
		return;
	}

	uint8_t speed = atoi(buffer);

	// Validate input
	if (speed > 180) {
		return;
	}

	DriveMotors(speed);
}

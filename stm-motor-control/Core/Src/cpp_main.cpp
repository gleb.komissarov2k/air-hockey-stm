#include "cpp_main.h"


#include "motors.h"
#include "encoder.h"
#include "stdio.h"
#include "math.h"

// Global variables
uint8_t g_uart_buffer[4] = { 0 };

Motors motors(&htim1, TIM_CHANNEL_1, &htim1, TIM_CHANNEL_4);
AS5048A encoder(&hspi1, SPI1_CS_GPIO_Port, SPI1_CS_Pin);

void cpp_main()
{
  // Start timers and PWM signals
  HAL_TIM_Base_Start(&htim1); // &htim1 is for motor signal PWM
  HAL_TIM_Base_Start(&htim2); // &htim2 is a millisecond clock
  HAL_TIM_Base_Start_IT(&htim3); // &htim2 is a 200Hz interrupt for encoder

  HAL_TIM_PWM_Start(&htim1, TIM_CHANNEL_1);
  HAL_TIM_PWM_Start(&htim1, TIM_CHANNEL_4);

  // Arm UART interrupt
  HAL_UART_Receive_IT(&huart2, g_uart_buffer, 3);
}

void cpp_loop()
{
	uint16_t angle = encoder.ReadRawAngle();
	char debug[50] = { 0 };
	sprintf(debug, "Raw angle: %d\n\r", angle);
	DebugData(debug);

	HAL_Delay(100);
	return;
}

void DebugData(char message[])
{
	HAL_UART_Transmit(&huart2, (uint8_t *) message, strlen(message), HAL_TIMEOUT);
}

uint32_t millis()
{
	return __HAL_TIM_GET_COUNTER(&htim2);
}


extern "C" void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
	// For debug prettiness
	uint8_t newline[2] = { '\n', '\r' };
	HAL_UART_Transmit(&huart2, newline, 2, 100);

	motors.ParseCommand((char *) g_uart_buffer);
	HAL_UART_Receive_IT(&huart2, g_uart_buffer, 3);
}

extern "C" void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
	if (GPIO_Pin == LEFT_SWITCH_Pin)
	{
		if (motors.speed < 90) {
			motors.DriveMotors(90);
			motors.position = 0;
			char debug[] = "Left Limit\n\r";
			DebugData(debug);
		}
	}

	else if (GPIO_Pin == RIGHT_SWITCH_Pin)
	{
		if (motors.speed > 90) {
			motors.DriveMotors(90);
			motors.position = motors.max_position;
			char debug[] = "Right Limit\n\r";
			DebugData(debug);
		}
	}
}

extern "C" void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
  // Read encoder
  if (htim == &htim3 )
  {
	  uint16_t new_position = encoder.ReadRawAngle();
	  uint16_t delta_x = abs(new_position - encoder.old_position);

	  // Going right
	  if (motors.speed > 90) {
		  if (new_position < motors.position) {
			  motors.position += 16384 - delta_x;
			  goto end_reading;
		  }
		  motors.position += delta_x;
		  goto end_reading;
	  }

	  // Going left
	  if (new_position > motors.position) {
		  motors.position -= 16384 - delta_x;
		  goto end_reading;
	  }
	  motors.position -= delta_x;
	  goto end_reading;

	  end_reading:
	  encoder.old_position = new_position;
  }
}

